package com.sicredi.backend.service;


import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.sicredi.backend.model.entity.Schedule;
import com.sicredi.backend.model.entity.Vote;
import com.sicredi.backend.repository.ScheduleRepository;
import com.sicredi.backend.repository.VoteRepository;

@SpringBootTest
class VoteServiceTests {

	@Autowired
    private VoteService voteService;

    @MockBean
    private ScheduleRepository scheduleRepository;
    
    @MockBean
    private VoteRepository voteRepository;

    @Test
    void shouldAddVote () throws Exception {
    	
    	Schedule schedule = getDummySchedule();
    	List<Vote> emptyList = new ArrayList<>();
    	Vote vote = getDummyVote(schedule);
    	
		doReturn(Optional.of(schedule )).when(scheduleRepository).findById(any(Long.class));
    	doReturn(emptyList).when(voteRepository).findByAssociateIdAndScheduleId(any(Long.class), any(Long.class));
       	doReturn(vote).when(voteRepository).save(any());
       	
       	Vote response = voteService.addVote(getVoteRequest(), 1l);
       	
       	assertNotNull(response, "Error return of the 'voteService.addVote' is null.");
       	assertNotNull(response.getId(), "Error voteId is null.");
       	assertNotNull(response.getAssociateId(), "Error associateId is null.");
       	assertNotNull(response.getValue(), "Error voteValue is null.");
       	assertNotNull(response.getInsertDate(), "Error insertDate return is null.");

    }
    
	private Vote getVoteRequest() {
		Vote vote = new Vote();
		vote.setAssociateId(1l);
		vote.setValue(false);
		return vote;
	}

	private Schedule getDummySchedule() {
		Schedule schedule = new Schedule();
		schedule.setId(99l);
    	schedule.setDescription("Devemos criar testes automaticos?");

		schedule.setSessionStart(LocalDateTime.now().minusMinutes(15));
		schedule.setSessionEnd(LocalDateTime.now().plusMinutes(15));
	
		List<Vote> voteList = new ArrayList<>();
		voteList.add(getDummyVote(schedule));
		schedule.setVoteList(voteList);
    	
		return schedule;
	}

	private Vote getDummyVote(Schedule schedule) {
		Vote vote = new Vote();
		vote.setId(32l);
    	vote.setSchedule(schedule);
    	vote.setAssociateId(21l);
    	vote.setValue(true);
    	vote.setInsertDate(LocalDateTime.now().minusMinutes(15));
		return vote;
	}

}
