package com.sicredi.backend.service;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.sicredi.backend.controller.dto.ScheduleResultResponseDTO;
import com.sicredi.backend.mapper.ScheduleMapper;
import com.sicredi.backend.model.entity.Schedule;
import com.sicredi.backend.model.entity.Vote;
import com.sicredi.backend.repository.ScheduleRepository;

@SpringBootTest
class ScheduleServiceTests {

    @Autowired
    private ScheduleMapper mapper;

	@Autowired
    private ScheduleService scheduleService;

    @MockBean
    private ScheduleRepository scheduleRepository;
    
    @Test
    void shouldCreateScheduleTest () throws Exception {
    	Schedule schedule = getDummyScheduleSimple();
    	
    	doReturn(schedule).when(scheduleRepository).save(any());
    	
		Schedule scheduleResp = scheduleService.createSchedule(schedule.getDescription());
		
		assertNotNull(scheduleResp, "Error return of the 'scheduleService.createSchedule' is null.");
		assertEquals(schedule.getId(), scheduleResp.getId(), "Error creating schedule, scheduleId does not equal.");
    }

    @Test
    void shouldStatSession () throws Exception {
    	Schedule schedule = getDummyScheduleSimple();
    	Schedule scheduleWithDate = getDummyScheduleWithDate();
    	
    	doReturn(Optional.of(schedule)).when(scheduleRepository).findById(any(Long.class));
    	doReturn(scheduleWithDate).when(scheduleRepository).save(any());
    	
    	Schedule response = scheduleService.startSession(schedule.getId(), 15l);
    	
		assertNotNull(response, "Error return of the 'scheduleService.startSession' is null.");
    	assertNotNull(response.getSessionStart(), "Error starting session, start date is null");
    	assertNotNull(response.getSessionStart(), "Error starting session, end date is null");
    }

    @Test
    void shouldGetResultStatSession () throws Exception {
    	Schedule schedule = getDummyScheduleComplete();
    	
    	doReturn(Optional.of(schedule)).when(scheduleRepository).findById(any(Long.class));
 
    	Schedule response = scheduleService.getResult(schedule.getId());
    	
		assertNotNull(response, "Error return of the 'scheduleService.getResult' is null.");
    	assertNotNull(response.getVoteList(), "Error geting result, voteResult is null.");
    	assertFalse(response.getVoteList().isEmpty(), "Error geting result, voteResult is empty.");
    }
    
	private Schedule getDummyScheduleSimple() {
		return getDummySchedule(false, false);
	}

	private Schedule getDummyScheduleWithDate() {
		return getDummySchedule(true, false);
	}

	private Schedule getDummyScheduleComplete() {
		return getDummySchedule(true, true);
	}

	private Schedule getDummySchedule(boolean withDate, boolean withVote) {
		Schedule schedule = new Schedule();
		schedule.setId(99l);
    	schedule.setDescription("Devemos criar testes automaticos?");

    	if (withDate) {
    		schedule.setSessionStart(LocalDateTime.now().minusMinutes(15));
    		schedule.setSessionEnd(LocalDateTime.now().plusMinutes(15));
    	}
    	
    	if (withVote) {
    		List<Vote> voteList = new ArrayList<>();
    		voteList.add(getDummyVote(schedule));
    		schedule.setVoteList(voteList);
    	}
    	
		return schedule;
	}

	private Vote getDummyVote(Schedule schedule) {
		Vote vote = new Vote();
		vote.setId(32l);
    	vote.setSchedule(schedule);
    	vote.setAssociateId(21l);
    	vote.setValue(true);
    	vote.setInsertDate(LocalDateTime.now());
		return vote;
	}
    
}
