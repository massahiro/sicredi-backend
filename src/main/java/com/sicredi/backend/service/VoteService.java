package com.sicredi.backend.service;

import com.sicredi.backend.model.entity.Vote;

public interface VoteService {
	
	Vote addVote(Vote request, Long scheduleId);
	
}
