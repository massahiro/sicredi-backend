package com.sicredi.backend.service.impl;

import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sicredi.backend.model.entity.Schedule;
import com.sicredi.backend.repository.ScheduleRepository;
import com.sicredi.backend.service.ScheduleService;
import com.sicredi.backend.service.exception.InvalidScheduleException;

@Service
public class ScheduleServiceImpl implements ScheduleService {

    private static final Logger log = LoggerFactory.getLogger(ScheduleServiceImpl.class);

    @Autowired
    private ScheduleRepository scheduleRepository;

	@Override
	public Schedule createSchedule(String description) {
		Schedule schedule = new Schedule();
		schedule.setDescription(description);
		Schedule savedSchedule = scheduleRepository.save(schedule);
		
		log.info("Schedule created with success {}, {}", savedSchedule.getId(), savedSchedule.getDescription());
		
		return savedSchedule;
	}

	@Override
	public Schedule startSession(Long scheduleId, Long duration) {
		if (duration == null || duration < 1) {
			duration = 1l;
		} 
		
		Optional<Schedule> optSchedule = scheduleRepository.findById(scheduleId);
		
		Schedule response = null;
		if (optSchedule.isPresent()) {
			Schedule schedule = optSchedule.get();
			
			if (schedule.getSessionStart() == null && schedule.getSessionEnd() == null) {
				schedule.setSessionStart(LocalDateTime.now());
				schedule.setSessionEnd(LocalDateTime.now().plusMinutes(duration));
				response = scheduleRepository.save(schedule);
				
				log.info("Session started with success to schedule {}, start {}, end {}", response.getId(), response.getSessionStart(), response.getSessionEnd());
			} else {
				log.error("Invalid parameter 'scheduleId' {}, it is already started or finished.", scheduleId);
				throw new InvalidScheduleException(scheduleId, "Related session is already started or finished.");
			}
			
		} else {
			log.error("Invalid parameter 'scheduleId' {}, it does not exists in database.", scheduleId);
			throw new InvalidScheduleException(scheduleId, "It does not exists in database.");
		}
		
		return response;
	}

	@Override
	public Schedule getResult(Long scheduleId){
		Optional<Schedule> optSchedule = scheduleRepository.findById(scheduleId);
		
		Schedule response = null;
		if (optSchedule.isPresent()) {
			response = optSchedule.get();
			log.info("Generated result with success for schedule {}", response.getId());
		} else {
			log.error("Invalid parameter 'scheduleId' {}, it does not exists in database.", scheduleId);
			throw new InvalidScheduleException(scheduleId, "It does not exists in database.");
		}

		return response;
	}

}
