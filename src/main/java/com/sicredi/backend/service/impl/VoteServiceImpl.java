package com.sicredi.backend.service.impl;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sicredi.backend.model.entity.Schedule;
import com.sicredi.backend.model.entity.Vote;
import com.sicredi.backend.repository.ScheduleRepository;
import com.sicredi.backend.repository.VoteRepository;
import com.sicredi.backend.service.VoteService;
import com.sicredi.backend.service.exception.InvalidCpfException;
import com.sicredi.backend.service.exception.InvalidScheduleException;
import com.sicredi.backend.service.exception.InvalidVoteException;
import com.sicredi.backend.validator.CpfValidator;

@Service
public class VoteServiceImpl implements VoteService {

    @Autowired
    private ScheduleRepository scheduleRepository;
    
    @Autowired
    private VoteRepository voteRepository;

    private static final Logger log = LoggerFactory.getLogger(VoteServiceImpl.class);

	@Override
	public Vote addVote(Vote request, Long scheduleId) {
		
		validateCPF(request.getAssociateCPF());

		Vote response = null;
		
		Optional<Schedule> optSchedule = scheduleRepository.findById(scheduleId);
		
		if (optSchedule.isPresent()) {
			Schedule schedule = optSchedule.get();
			
			validateSession(schedule);
			
			List<Vote> voteList = voteRepository.findByAssociateIdAndScheduleId(
					request.getAssociateId(), scheduleId);
			
			if (voteList != null && !voteList.isEmpty()) {
				log.error("The 'associateId' {} already voted to this 'scheduderId' {}.", request.getAssociateId(), scheduleId);
				throw new InvalidVoteException();
			} 
			
			request.setSchedule(schedule);
			request.setInsertDate(LocalDateTime.now());
			
			response = voteRepository.save(request);
			
			log.info("Vote {} counted with success, 'associateId' {}, 'scheduleId' {}", response.getValue(), response.getAssociateId(), response.getSchedule().getId());
		} else {
			log.error("Invalid parameter 'scheduleId' {}, it does not exists in database.", scheduleId);
			throw new InvalidScheduleException(scheduleId, "It does not exists in database.");
		}
		
		return response;
	}

	private void validateCPF(String cpf) {
		if (StringUtils.isNotBlank(cpf) && !CpfValidator.isValidCpf(cpf)) {
			log.error("Invalid parameter 'associateCpf {}", cpf);
			throw new InvalidCpfException(cpf) ;
		}
	}

	private void validateSession(Schedule schedule) {
		LocalDateTime today = LocalDateTime.now();
		
		if (schedule.getSessionStart() == null && schedule.getSessionEnd() == null) {
			log.error("Voting session has not started for the 'sheduleId' {}.", schedule.getId());
			throw new InvalidScheduleException(schedule.getId(), "Voting session has not started for related shedule'.");
			
		} else if (today.isAfter(schedule.getSessionEnd()) ){
			log.error("Voting session has finished {} for the 'sheduleId'.", schedule.getSessionEnd());
			throw new InvalidScheduleException(schedule.getId(), "Voting session has finished for related shedule.");
		}
		
	}

}
