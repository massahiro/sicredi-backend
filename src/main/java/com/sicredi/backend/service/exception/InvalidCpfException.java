package com.sicredi.backend.service.exception;

public class InvalidCpfException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	private static String messageError = "Invalid CPF %s. ";
	
	private String cpf;
	
	private String addicionalMsg = "";

	public InvalidCpfException(String cpf) {
		super();
		this.cpf = cpf;
	}

	public InvalidCpfException(String cpf, String addicionalMsg) {
		super();
		this.cpf = cpf;
		this.addicionalMsg = addicionalMsg;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getAddicionalMsg() {
		return addicionalMsg;
	}

	public void setAddicionalMsg(String addicionalMsg) {
		this.addicionalMsg = addicionalMsg;
	}

	public String getMsg () {
		return String.format(messageError, cpf) + addicionalMsg;
	}
}
