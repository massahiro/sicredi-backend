package com.sicredi.backend.service.exception;

public class InvalidScheduleException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	private static String messageError = "Invalid schedule with ID %d. ";
	
	private Long scheduleId;
	
	private String addicionalMsg = "";

	public InvalidScheduleException(Long scheduleId) {
		super();
		this.scheduleId = scheduleId;
	}

	public InvalidScheduleException(Long scheduleId, String addicionalMsg) {
		super();
		this.scheduleId = scheduleId;
		this.addicionalMsg = addicionalMsg;
	}

	public long getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(Long scheduleId) {
		this.scheduleId = scheduleId;
	}

	public String getAddicionalMsg() {
		return addicionalMsg;
	}

	public void setAddicionalMsg(String addicionalMsg) {
		this.addicionalMsg = addicionalMsg;
	}

	public String getMsg () {
		return String.format(messageError, scheduleId) + addicionalMsg;
	}
}
