package com.sicredi.backend.service.exception;

public class InvalidVoteException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	private static String messageError = "Invalid Vote. Each associate can vote only once each schedule.";
	
	private String addicionalMsg = "";

	public InvalidVoteException() {
		super();
	}

	public InvalidVoteException(String cpf, String addicionalMsg) {
		super();
		this.addicionalMsg = addicionalMsg;
	}

	public String getAddicionalMsg() {
		return addicionalMsg;
	}

	public void setAddicionalMsg(String addicionalMsg) {
		this.addicionalMsg = addicionalMsg;
	}

	public String getMsg () {
		return messageError + addicionalMsg;
	}
}
