package com.sicredi.backend.service.exception;

public class ValidatingCpfException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	private static String messageError = "Not found.";
	
	private String addicionalMsg = "";

	public ValidatingCpfException() {
		super();
	}

	public ValidatingCpfException(String cpf, String addicionalMsg) {
		super();
		this.addicionalMsg = addicionalMsg;
	}

	public String getAddicionalMsg() {
		return addicionalMsg;
	}

	public void setAddicionalMsg(String addicionalMsg) {
		this.addicionalMsg = addicionalMsg;
	}

	public String getMsg () {
		return messageError + addicionalMsg;
	}
}
