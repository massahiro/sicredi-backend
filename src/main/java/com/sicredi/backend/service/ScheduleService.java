package com.sicredi.backend.service;

import com.sicredi.backend.model.entity.Schedule;

public interface ScheduleService {

	Schedule createSchedule(String description);
	
	Schedule startSession(Long scheduleId, Long duration); 

	Schedule getResult(Long scheduleId); 

	
}
