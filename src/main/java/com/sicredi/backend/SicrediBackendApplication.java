package com.sicredi.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SicrediBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(SicrediBackendApplication.class, args);
	}

}
