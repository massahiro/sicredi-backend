package com.sicredi.backend.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sicredi.backend.controller.dto.ScheduleRequestDTO;
import com.sicredi.backend.controller.dto.ScheduleResponseDTO;
import com.sicredi.backend.controller.dto.ScheduleResultResponseDTO;
import com.sicredi.backend.mapper.ScheduleMapper;
import com.sicredi.backend.service.ScheduleService;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("api/v1/schedule")
public class ScheduleController {
	
    private static final Logger log = LoggerFactory.getLogger(ScheduleController.class);
    
	@Autowired
	private ScheduleService scheduleService;
	
    @Autowired
    private ScheduleMapper mapper;

	@ApiOperation(value = "Creates a new shedule.", response = ScheduleResponseDTO.class)
	@ApiResponses({ @ApiResponse(code = 201, message = "Created", response = ScheduleResponseDTO.class),
		@ApiResponse(code = 400, message = "Bad Request"),
		@ApiResponse(code = 500, message = "Internal Server Error") })
	@ApiImplicitParams({@ApiImplicitParam(name = "scheduleRequest", value = "Request with 'description' is required.", required = true, dataTypeClass = ScheduleRequestDTO.class, paramType = "body")})
    @PostMapping
    public ResponseEntity<ScheduleResponseDTO> createSchedule(
            @RequestBody @Valid ScheduleRequestDTO scheduleRequest) {

        log.info("Creating schedule {} ", scheduleRequest.getDescription());

        ScheduleResponseDTO resp = mapper.mapResponse(scheduleService.createSchedule(scheduleRequest.getDescription()));
        
        return ResponseEntity.status(HttpStatus.CREATED).body(resp);
    }

	@ApiOperation(value = "Starts the voting session of the schedule.", response = ScheduleResponseDTO.class)
	@ApiResponses({ @ApiResponse(code = 200, message = "Ok"),
		@ApiResponse(code = 400, message = "Bad Request"),
		@ApiResponse(code = 500, message = "Internal Server Error") })
	@ApiImplicitParams({@ApiImplicitParam(name = "scheduleId", value = "Id of referente schedule.", example = "1", required = true, dataTypeClass = Long.class, paramType = "path"),
		@ApiImplicitParam(name = "duration", value = "Session duration time in minutes.", example = "1", dataTypeClass = Long.class, paramType = "query")})
    @PutMapping("/{scheduleId}/startsession")
    public ResponseEntity<ScheduleResponseDTO> startSession(
            @PathVariable("scheduleId") Long scheduleId,
            @RequestParam(value="duration", required = false) Long duration) {

        log.info("Starting session, schedule {}, duration {} minutes", scheduleId, duration);

        ScheduleResponseDTO resp = mapper.mapResponse(scheduleService.startSession(scheduleId, duration));

        return ResponseEntity.status(HttpStatus.OK).body(resp);
    }

	@ApiOperation(value = "Gets the result of the voting of the schedule.", response = ScheduleResultResponseDTO.class)
	@ApiResponses({ @ApiResponse(code = 200, message = "Ok", response = ScheduleResultResponseDTO.class),
		@ApiResponse(code = 400, message = "Bad Request"),
		@ApiResponse(code = 500, message = "Internal Server Error") })
	@ApiImplicitParams({@ApiImplicitParam(name = "scheduleId", value = "Id of referente schedule.", example = "1", required = true, dataTypeClass = Long.class, paramType = "path")})
    @GetMapping("/{scheduleId}/result")
    public ResponseEntity<ScheduleResultResponseDTO> getResult(
    		@PathVariable("scheduleId") Long scheduleId) {

        log.info("Geting result schedule {}", scheduleId);
        
        ScheduleResultResponseDTO resp = mapper.mapResultResponse(scheduleService.getResult(scheduleId));

        return ResponseEntity.status(HttpStatus.OK).body(resp);
    }

}
