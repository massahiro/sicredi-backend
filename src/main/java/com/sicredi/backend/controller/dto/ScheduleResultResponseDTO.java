package com.sicredi.backend.controller.dto;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class ScheduleResultResponseDTO extends ScheduleResponseDTO {
	
	private Map<String, Long> voteResult;

	public Map<String, Long> getVoteResult() {
		return voteResult;
	}

	public void setVoteResult(Map<String, Long> voteResult) {
		this.voteResult = voteResult;
	}

}
