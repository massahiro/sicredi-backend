package com.sicredi.backend.controller.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class VoteRequestDTO {
	
	@Min(1)
	private Long associateId;

	private String associateCpf;
	
	@NotNull
	private Boolean voteValue;

	@Min(1)
	private Long scheduleId;

	public Long getAssociateId() {
		return associateId;
	}

	public void setAssociateId(Long associateId) {
		this.associateId = associateId;
	}

	public String getAssociateCpf() {
		return associateCpf;
	}

	public void setAssociateCpf(String associateCpf) {
		this.associateCpf = associateCpf;
	}

	public Boolean getVoteValue() {
		return voteValue;
	}

	public void setVoteValue(Boolean voteValue) {
		this.voteValue = voteValue;
	}

	public Long getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(Long scheduleId) {
		this.scheduleId = scheduleId;
	}

}
