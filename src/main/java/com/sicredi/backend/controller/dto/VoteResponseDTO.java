package com.sicredi.backend.controller.dto;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class VoteResponseDTO {

	private Long voteId;
	
	private Long associateId;
	
	private Boolean voteValue;

	private LocalDateTime insertDate;
	
	private ScheduleResponseDTO scheduleResponse;

	public Long getVoteId() {
		return voteId;
	}

	public void setVoteId(Long voteId) {
		this.voteId = voteId;
	}

	public Long getAssociateId() {
		return associateId;
	}

	public void setAssociateId(Long associateId) {
		this.associateId = associateId;
	}

	public ScheduleResponseDTO getScheduleResponse() {
		return scheduleResponse;
	}

	public void setScheduleResponse(ScheduleResponseDTO scheduleResponse) {
		this.scheduleResponse = scheduleResponse;
	}

	public Boolean getVoteValue() {
		return voteValue;
	}

	public void setVoteValue(Boolean voteValue) {
		this.voteValue = voteValue;
	}

	public LocalDateTime getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(LocalDateTime insertDate) {
		this.insertDate = insertDate;
	}
	
}
