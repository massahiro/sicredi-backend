package com.sicredi.backend.controller.dto;

import javax.validation.constraints.NotBlank;

public class ScheduleRequestDTO {
	
	@NotBlank
	private String description;

	public ScheduleRequestDTO() {
		super();
	}

	public ScheduleRequestDTO(String description) {
		super();
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
