package com.sicredi.backend.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sicredi.backend.controller.dto.VoteRequestDTO;
import com.sicredi.backend.controller.dto.VoteResponseDTO;
import com.sicredi.backend.mapper.VoteMapper;
import com.sicredi.backend.model.entity.Vote;
import com.sicredi.backend.service.VoteService;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("api/v1/vote")
public class VoteController {
	
    private static final Logger log = LoggerFactory.getLogger(VoteController.class);

	@Autowired
	private VoteMapper mapper;

	@Autowired
	private VoteService voteService;

	@ApiOperation(value = "Adds a vote ('SIM'/'NAO') to the opened schedule.", response = VoteResponseDTO.class)
	@ApiResponses({ @ApiResponse(code = 200, message = "Ok", response = VoteResponseDTO.class),
		@ApiResponse(code = 400, message = "Bad Request"),
		@ApiResponse(code = 404, message = "Not Found"),
		@ApiResponse(code = 500, message = "Internal Server Error") })
	@ApiImplicitParams({@ApiImplicitParam(name = "voteRequest", value = "Request with voteRequest, associateId, scheduleId is required, and cpf ís optional.", required = true, dataTypeClass = VoteRequestDTO.class, paramType = "body") })
    @PostMapping
    public ResponseEntity<VoteResponseDTO> addVote(
            @RequestBody @Valid VoteRequestDTO voteRequest) {

		log.info("Adding vote {}, from {} to Schedule {}", voteRequest.getVoteValue(), voteRequest.getAssociateId(), voteRequest.getScheduleId());

		Vote req = mapper.map(voteRequest);
		
        return ResponseEntity.status(HttpStatus.OK).body(mapper.mapResponse(voteService.addVote(req, voteRequest.getScheduleId())));
    }

}
