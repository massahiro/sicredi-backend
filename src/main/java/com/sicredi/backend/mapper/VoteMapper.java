package com.sicredi.backend.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sicredi.backend.controller.dto.VoteRequestDTO;
import com.sicredi.backend.controller.dto.VoteResponseDTO;
import com.sicredi.backend.model.entity.Vote;

@Component
public class VoteMapper {

	@Autowired
	private ScheduleMapper mapper;
	
	public Vote map(VoteRequestDTO input) {
		Vote output = new Vote();
		output.setAssociateId(input.getAssociateId());
		output.setAssociateCPF(input.getAssociateCpf());
		output.setValue(input.getVoteValue());
		return output;
	}

	public VoteResponseDTO mapResponse(Vote input) {
		VoteResponseDTO output = new VoteResponseDTO();
		output.setVoteId(input.getId());
		output.setAssociateId(input.getAssociateId());
		output.setVoteValue(input.getValue());
		output.setInsertDate(input.getInsertDate());
		output.setScheduleResponse(mapper.mapResponse(input.getSchedule()));
		return output;
	}
}
