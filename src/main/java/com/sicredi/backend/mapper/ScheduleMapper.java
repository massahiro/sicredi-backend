package com.sicredi.backend.mapper;

import java.util.HashMap;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Component;

import com.sicredi.backend.controller.dto.ScheduleResponseDTO;
import com.sicredi.backend.controller.dto.ScheduleResultResponseDTO;
import com.sicredi.backend.model.entity.Schedule;
import com.sicredi.backend.model.entity.Vote;

@Component
public class ScheduleMapper {

	public ScheduleResponseDTO mapResponse(Schedule input) {
		ScheduleResponseDTO response = new ScheduleResponseDTO();
		response.setId(input.getId());
		response.setDescription(input.getDescription());
		response.setSessionStart(input.getSessionStart());
		response.setSessionEnd(input.getSessionEnd());
		
		return response ;
	}
	
	public ScheduleResultResponseDTO mapResultResponse(Schedule input) {
		ScheduleResultResponseDTO response = new ScheduleResultResponseDTO();
		response.setId(input.getId());
		response.setDescription(input.getDescription());
		response.setSessionStart(input.getSessionStart());
		response.setSessionEnd(input.getSessionEnd());
		response.setVoteResult(new HashMap<>());

		if (!CollectionUtils.isEmpty(input.getVoteList())) {
			List<Vote> voteList = input.getVoteList();
			Long yes = voteList.stream().filter(ft -> ft.getValue()).count();
			Long no = voteList.stream().filter(ft -> !ft.getValue()).count();
			response.getVoteResult().put("SIM", yes);
			response.getVoteResult().put("NAO", no);
		}
		return response;
	}
}
