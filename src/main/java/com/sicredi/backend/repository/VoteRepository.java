package com.sicredi.backend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sicredi.backend.model.entity.Vote;

public interface VoteRepository extends JpaRepository<Vote, Long> {

	List<Vote> findByAssociateIdAndScheduleId(Long associateId, Long scheduleId);
}
