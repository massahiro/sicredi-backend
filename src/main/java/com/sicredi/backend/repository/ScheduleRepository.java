package com.sicredi.backend.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sicredi.backend.model.entity.Schedule;

public interface ScheduleRepository extends JpaRepository<Schedule, Long> {

	Schedule findByIdAndSessionStartLessThanEqualAndSessionEndGreaterThanEqual(Long id, Date date, Date date1);
	
}
