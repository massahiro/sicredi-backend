package com.sicredi.backend.validator;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.sicredi.backend.service.exception.ValidatingCpfException;

public class CpfValidator {
	
    private static final Logger log = LoggerFactory.getLogger(CpfValidator.class);

	private static final String ABLE_TO_VOTE = "ABLE_TO_VOTE";
	private static String URL = "https://user-info.herokuapp.com/users/%s";
	
	public static boolean isValidCpf(String cpf) {
		boolean response = false;
		
		try {
			//somente digitos
			cpf = cpf.replaceAll("\\D+","");
			
			RestTemplate restTemplate = new RestTemplate();
			
			ResponseEntity<CpfValidatorResponse> cpfResp = restTemplate.getForEntity(
					String.format(URL, cpf), CpfValidatorResponse.class);
			
			if (cpfResp.getStatusCode().equals(HttpStatus.OK) 
					&& cpfResp.getBody() != null) {
				CpfValidatorResponse bodyResp = cpfResp.getBody();
				
				if (bodyResp != null && StringUtils.equals(bodyResp.getStatus(), ABLE_TO_VOTE)) {
					response = true;
				}
			}
		} catch (HttpClientErrorException e) {
			log.error("Error validating cpf", e);
			throw new ValidatingCpfException();
		}
		
		return response;
	} 
	
}
