package com.sicredi.backend.validator;

public class CpfValidatorResponse {
	
	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
