# SICREDI Backend v1

### Sobre o Projeto

Este projeto foi desenvolvido com a finalidade de avalição técnica.
A aplicação tem o objetivo de simular uma assembleia. Onde se tomam decisões sobre uma determinada pauta, através de votação.

As funcionalidades são:

- Criar uma pauta.
- Iniciar a sessão de votação uma pauta, que ficará aberta por um determinado tempo.
- Realizar um voto.
- Consultar o resultado da votação de uma pauta.


### Tecnologias utilizadas:

- **Java**: Uma das linguagens de programação mais utilizadas pelo mercado, que tem evolido muito ao longo dos anos. Linguagens robusta que permite alta produtividade, ótima segurança e tem uma imensa gama de frameworks.
- **Spring Framework**: Um principal framework de Java utilizado atualmente. Permite maior produtividade, mais agilidade no desenvolvimento, maior segurança, entro outros.
- **JPA**: É um framework de persistência muito utilizado no mercado. A grande ideia da especificação JPA é que a aplicação possa trocar de implementação sem que precise de mudanças no código. Apenas um pouco de configuração.
- **H2 Data Base**: Baco de dados simples fácil de usar e configurar. Pode ser usado com dados em memória e em arquivo. E possui dashboard web.
- **Junit e Mockito**: Frameworks padrões o para testes. Facilita a criação, execução automática de testes e a apresentação dos resultados
- **Gradle**: Melhor: gerência de dependência do projeto, compilação do projeto, geração de pacotes, automação de tarefas e ampla integração com outros sistemas de automação.
- **Git**: Repositório de código fonte. Possui boa performance segurança e flexibilidade.

## Instalação e execução do projeto

### Pré requisitos:
- Ter instalado o Git
- Ter instalado Java 8

Segue as instruções para instalar e executar o projeto:

1. Vá até uma pasta a sua escolha:
```
cd <workspace>
```

2. Faça o download do código fonte. Em um terminal execute o comando:
```
git clone https://bitbucket.org/massahiro/sicredi-backend.git
```

3. Entre na raiz do projeto:
```
cd <workspace>/sicredi-backend
```

4. Execute o “build” e os testes do projeto, com o seguinte comando (Gradle Wrapper está incluído no projeto): 

Para Linux:
```
./gradlew clean build
```

Para Windows:
```
gradlew.bat clean build 
```

Para Linux, **sem a execução dos testes**:
```
./gradlew clean build -x test
```

Para Windows, **sem a execução dos testes**:
```
gradlew.bat clean build -x test
```

5. Vá até a pasta 'libs', com o comando
```
cd <workspace>/sicredi-backend/build/libs
```

6. Execute a aplicação, com o comando:
```
java -jar sicredi-backend-1.0.0-SNAPSHOT.jar
```


## Exemplos de utilização da API

A Aplicação pode ser testar a partir da interface do usuário do Swagger através do link:
```
http://localhost:8080/swagger-ui/
```

Ou atravéz da Terminal do Linux, seguem os exemplos abaixo.

1. Criar uma pauta:

Request:
```
curl -X POST "http://localhost:8080/api/v1/schedule/" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"description\": \"Devemos desenvolver testes automaticos?\"}"
```

Response:
```
{
  "id": 78,
  "description": "Devemos desenvolver testes automaticos?"
}
```

2. Iniciar uma sessão de votação de uma pauta:

Request:
```
curl -X PUT "http://localhost:8080/api/v1/schedule/78/startsession?duration=30" -H "accept: application/json"
```

Response:
```
{
  "id": 78,
  "description": "Devemos desenvolver testes automaticos?",
  "sessionStart": "2021-04-09T12:35:46.943+00:00",
  "sessionEnd": "2021-04-09T13:05:46.943+00:00"
}
```

3. Votar um uma sessão aberta:

Request:
```
curl -X POST "http://localhost:8080/api/v1/vote/" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"associateId\": 1, \"scheduleId\": 78, \"voteValue\": true }"
```

Response:
```
{
  "voteId": 79,
  "associateId": 1,
  "vote": true,
  "insertDate": "2021-04-09T12:37:16.153+00:00",
  "scheduleResponse": {
    "id": 78,
    "description": "Devemos desenvolver testes automaticos?",
    "sessionStart": "2021-04-09T12:35:46.943+00:00",
    "sessionEnd": "2021-04-09T13:05:46.943+00:00"
  }
}
```

4. Consultar o resultado da votação da pauta:

Request:
```
curl -X GET "http://localhost:8080/api/v1/schedule/78/result" -H "accept: application/json"
```

Response:
```
{
  "id": 78,
  "description": "Devemos desenvolver testes automaticos?",
  "sessionStart": "2021-04-09T12:35:46.943+00:00",
  "sessionEnd": "2021-04-09T13:05:46.943+00:00",
  "voteResult": {
    "SIM": 1,
    "NAO": 0
  }
}
```

## Acesso ao banco de dados

Para acessar o dashboard do banco de dados H2. Acesso o link:

```
http://localhost:8080/h2
```

Verifique os dados de conexão:

```
Driver Class: org.h2.Driver
JDBC URL: jdbc:h2:file:./src/main/resources/data/filedb
User Name: sa
Password:
```

Obs: Password não precisa ser preechido.

Preenchido os dados de conexão, clique no botão "Connect".

Agora é possivel consultar as tabelas para verificar os dados resgistrados pela aplicação.



## Autor
Leandro Massahiro Ogusuku